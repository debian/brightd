brightd (0.4.1-4) unstable; urgency=medium

  * Add DEP-3 headers, rename the FTCBFS patch.
  * Stop inlining functions used multiple times (Closes: #957057).

 -- Andrej Shadura <andrewsh@debian.org>  Mon, 21 Sep 2020 17:18:47 +0200

brightd (0.4.1-3) unstable; urgency=medium

  [ Andrej Shadura ]
  * Actually compare the value read out of /sys/class/power_supply/AC/online
    with '0' (Closes: #934140).
  * Use debhelper-compat instead of debian/compat.
  * d/control: Set Vcs-* to salsa.debian.org.
  * Use secure URI in debian/watch.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Provide machine-readable debian/copyright.
  * Machine-readable debian/copyright

  [ Helmut Grohne ]
  * Fix FTCBFS (Closes: #923266):
    + cross.patch: Make compiler and install substitutable.
    + Let dh_auto_build pass a cross compiler to make.
    + Pass a non-stripping install to make install.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 11 Dec 2019 14:12:49 +0100

brightd (0.4.1-2) unstable; urgency=medium

  * Refresh the patch.
  * Don’t use deprecated /proc/acpi for AC detection
    (Closes: #754494, #757012).
  * Include the reference to the bug number into the patch.
  * Use int for brightness, not char (Closes: #894164).
  * Fix FTBFS with binutils-gold / --as-needed (Closes: #751571).
  * Reword the contradiction in the -e option description (Closes: #799368).
  * Add Vcs-* pointing to dgit.

 -- Andrej Shadura <andrewsh@debian.org>  Wed, 05 Sep 2018 14:48:36 +0200

brightd (0.4.1-1) unstable; urgency=low

  * Initial release, pick up from an old package by
    Evgeni Golov <evgeni> (Closes: #419329).

 -- Andrew Shadura <andrewsh@debian.org>  Fri, 07 Feb 2014 23:55:28 +0100
